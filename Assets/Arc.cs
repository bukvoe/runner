using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arc : MonoBehaviour
{
    private List<GameObject> _objects;
    
    [Header("Position")]
    [SerializeField] private int startPosition;
    [SerializeField] private int endPosition;
    
    [SerializeField] private float endHeight;
    
    [Header("Angle")]
    [SerializeField] private float startAngle;
    [SerializeField] private float endAngle;
    
    [SerializeField] private GameObject _gameObject;

    private void OnValidate()
    {
        if (_objects != null)
        {
            foreach (var item in _objects)
            {
                UnityEditor.EditorApplication.delayCall+=()=>
                {
                    DestroyImmediate(item);
                };
            }
        }
        _objects ??= new List<GameObject>();

        for (var spawnIndex = startPosition; spawnIndex < endPosition; spawnIndex++)
        {
            var newObject = Instantiate(_gameObject, Vector3.right * spawnIndex + Vector3.up * endHeight * spawnIndex / endPosition, _gameObject.transform.rotation);
            _objects.Add(newObject);

            Debug.Log(startAngle + (endAngle - startAngle) * ((spawnIndex + 1) / endPosition));
            
            newObject.transform.Rotate(
                _gameObject.transform.rotation.x,
                _gameObject.transform.rotation.y,
                (float)startAngle + (float)(endAngle - startAngle) * ((float)(spawnIndex + 1) / (float)endPosition));
        }
    }

    private Vector3 CalculatePosition(float value)
    {
        
        return Vector3.back;
    }
}
