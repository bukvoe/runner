using System;
using System.Collections;
using System.Collections.Generic;
using Model;
using UnityEngine;
using UnityEngine.UI;

public class Scroll : MonoBehaviour
{
    [SerializeField] private GameObject _platform;
    private Scrollbar _scrollbar;
    private SliderButton _button;
    private Character _character;
    private float _rotation;
    private float _pos;
    private float _prevPos;
    private GameObject prev;

    private Vector3 _next;


    private void Start()
    {
        _character = FindObjectOfType<Character>();
        _scrollbar = GetComponent<Scrollbar>();
        //_scrollbar.onValueChanged.AddListener(SpawnObject);
        //_button = _scrollbar.GetComponentInChildren<SliderButton>();
        //_button._onButtonPressed.AddListener(SpawnObject);
        //_pos = _character.transform.position.x;
        _rotation = 0;
        //_scrollbar.OnDrag(SpawnObject(0));
    }


    public float SetValue(float value)
    {
        if (value > 1) value = 1;
        if (value < -1) value = -1;
        
        if (value == 0)
        {
            value = 0.5f;
        }
        else
        {
            value = 0.5f + value * 0.5f;
        }
        
        //Debug.Log(value);
        _scrollbar.value = value;
        _prevPos = value;
        return value;
    }

    // private void SpawnObject(float value)
    // {
    //
    //     
    //     
    //     if(_pos - _character.transform.position.x < 500f)
    //     {
    //         var coef = 1;
    //         var current = _rotation;
    //         
    //         if (value == 0.5f)
    //         {
    //             _rotation = 0;
    //         }
    //         else if (value > 0.5f && _rotation < 30f)
    //         {
    //             _rotation += (value - 0.5f) * 20f;
    //             Debug.Log($"ROTATION" + _rotation);
    //         }
    //         else if (value < 0.5f &&  _rotation > -30f)
    //         {
    //             _rotation -= value * 20f;
    //             coef = -1;
    //         }
    //
    //         var max = 20;
    //         var delta = Math.Abs(_rotation - current);
    //         for (int i = 0; i < max; i++)
    //         {
    //             var platform = Instantiate(_platform, new Vector3(
    //                     _pos,
    //                     _character.transform.position.y + coef *  i * delta / 10,
    //                     _character.transform.position.z), 
    //                 _platform.transform.rotation);
    //         
    //             platform.transform.Rotate(transform.rotation.x, transform.rotation.y, current + coef * delta * i / max);
    //         
    //             _pos += _platform.transform.localScale.x + 0.1f;
    //         }
    //         
    //     }
    // }
    

    // private void SpawnObject(float value)
    // {    
    //     var deltaHeight = Math.Abs(_character.transform.position.y - _scrollbar.value * 10);
    //     var deltaWidth = 1 / deltaHeight;
    //     if (deltaHeight > 1)
    //     {
    //         var direction = _character.transform.position.y - _scrollbar.value * 10 <= 0 ? 1 : -1;
    //
    //         for (int i = 0; i < deltaHeight; i++)
    //         {
    //             Instantiate(_platform, _character.transform.position + Vector3.up * i * direction + Vector3.right * deltaWidth * i, _platform.transform.rotation);
    //         }
    //     }
    //     else
    //     {
    //         Instantiate(_platform, _character.transform.position, _platform.transform.rotation);
    //
    //     }    
    //     
    //     _character.transform.position = new Vector3(_character.transform.position.x, _scrollbar.value * 10, _character.transform.position.z);
    //     //_spawnPosition += Ve _character.transform.position.y;
    // }
}
