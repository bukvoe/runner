using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SliderButton : MonoBehaviour, IPointerDownHandler, IPointerExitHandler
{
        public UnityEvent<float> _onButtonPressed;
    
        private bool _buttonPressed;

        private void Update()
        {
            if (_buttonPressed)
            {
                _onButtonPressed.Invoke(0f);
            }
        }

        public void OnPointerDown(PointerEventData eventData){
            Debug.Log('d');
            _buttonPressed = true;
        }
 
        public void OnPointerExit(PointerEventData eventData){
            Debug.Log('u');
            _buttonPressed = false;
        }
}
