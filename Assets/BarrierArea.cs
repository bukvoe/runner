using System;
using System.Collections;
using System.Collections.Generic;
using Model;
using UnityEngine;
using Random = UnityEngine.Random;

public class BarrierArea : MonoBehaviour
{
    [SerializeField] private Barrier _barrier;
    private void Start()
    {
        var barriersAmount = Random.Range(1, 4);

        var increment = transform.localScale.z / (barriersAmount + 1);
        
        var currentPosition = transform.position.z - transform.localScale.z / 2 + increment;
        var position = transform.position;
        Debug.Log(transform.localScale.z);
        Debug.Log(currentPosition);
        Debug.Log(position);
        for (var barrierIndex = 0;  barrierIndex < barriersAmount; barrierIndex++, currentPosition += increment)
        {
            Debug.Log(currentPosition);
            Instantiate(_barrier, new Vector3(position.x, position.y, currentPosition), _barrier.transform.rotation);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Application.isPlaying ? new Color(0, 0, 0, 0) : Color.yellow;
        Gizmos.DrawCube(transform.position, transform.localScale);
    }
}
