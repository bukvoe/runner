﻿using System;
using Components;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace Model
{
    [RequireComponent(typeof(Rigidbody))]
    public class Character : MonoBehaviour
    {
        private Vector3 _normal;
        private Rigidbody _rigidbody;
        private Spawner _spawner;

        private float _directionPrev;

        [SerializeField] private Platform _platform;
        [SerializeField] private float _speed;
        [SerializeField] private float _vSpeed;
        [SerializeField] private PlayerInput _playerInput;

        private readonly float MaxDirection = Screen.height / 4f;



        [SerializeField] private float _maxY;
        [SerializeField] private float _maxAngle;
        private float _distanceBetweenPlatforms;
        private Vector3 _prevPosition;
        private Platform _prevObject;

        private void OnCollisionEnter(Collision collision)
        {
            _normal = collision.contacts[0].normal;
            // var angle = collision.contacts[0].otherCollider.gameObject.transform.rotation.eulerAngles.z;
            // if (angle != 0)
            // {
            //     transform.Rotate(-angle, transform.rotation.y, transform.rotation.z);
            // }

            //_rigidbody.constraints |= RigidbodyConstraints.FreezeRotationX;

            // transform.rotation = new Quaternion(
            //     -collision.contacts[0].otherCollider.gameObject.transform.rotation.z,
            //     transform.rotation.y,
            //     -collision.contacts[0].otherCollider.gameObject.transform.rotation.z,
            //     -collision.contacts[0].otherCollider.gameObject.transform.rotation.w
            // );
        }

        private void Start()
        {
            _spawner = GetComponentInChildren<Spawner>();
            

            _rigidbody = GetComponent<Rigidbody>();
            _rigidbody.velocity = Vector3.right * _speed;
            _rigidbody.maxAngularVelocity = 1f;
            
            _playerInput.DirectionChanged += OnDirectionChanged;
            _distanceBetweenPlatforms = _platform.transform.localScale.x * 1.25f;
            _prevPosition = transform.position;
            _prevObject = _platform;
        }

        private void OnDestroy()
        {
            _playerInput.DirectionChanged -= OnDirectionChanged;

        }

        private void Update()
        {
            Move(_rigidbody.velocity);
        }


        private void ChangeHeight(float direction)
        {
            if (Math.Abs(direction) > MaxDirection)
            {
                direction = direction >= 0 ? MaxDirection : -MaxDirection;
            }

            var deltaDirection = Math.Abs(direction - _directionPrev);

            if ((int) deltaDirection == 0)
            {
                deltaDirection = _directionPrev;
            }
            else
            {

                deltaDirection *= direction > _directionPrev ? 1 : -1;
                _directionPrev = direction;
            }


            var verticalVelocity = deltaDirection / MaxDirection * 2 * _speed;

            _rigidbody.velocity = Vector3.up * verticalVelocity + Vector3.right * _speed;
        }

        private void ChangeAngle(float direction)
        {
        }

        // private float GetSpeedByOffset(float offset)
        // {
        //     if (Math.Abs(offset) > MaxDirection)
        //     {
        //         offset = offset > 0 ? MaxDirection : -MaxDirection;
        //     }
        //     return offset / MaxDirection;
        // }
        //
        // private float GetAngleByOffset(float offset)
        // {
        //     if (Math.Abs(offset) > MaxDirection)
        //     {
        //         offset = offset > 0 ? MaxDirection : -MaxDirection;
        //     }
        //     
        //     
        //     return offset / MaxDirection;
        // }

        private float GetTargetHeight(float offset)
        {
            if (Math.Abs(offset) > MaxDirection)
            {
                offset = offset > 0 ? MaxDirection : -MaxDirection;
            }
            
            return offset / MaxDirection * _maxY;
        }
        
        private float GetTargetAngle(float offset)
        {
            if (Math.Abs(offset) > MaxDirection)
            {
                offset = offset > 0 ? MaxDirection : -MaxDirection;
            }
            
            return offset / MaxDirection * 3;
        }

        
        private void OnDirectionChanged(Vector2 direction)
        {
     

            // var targetAngle = GetTargetAngle(direction.y);
            //
            // if (transform.rotation.x + targetAngle >= 50 || transform.rotation.x + targetAngle <= -50)
            // {
            //     
            // }
            // else
            // {
            //     transform.Rotate(targetAngle,0,0);
            // }
        }

    // private void OnDirectionChanged(Vector2 direction)
        // {
        //     _rigidbody.velocity = new Vector3(
        //         _rigidbody.velocity.x,
        //         GetSpeedByOffset(direction.y) * _maxY,
        //         0);
        //    
        //     var angularSpeed = transform.rotation.x > _maxAngle && transform.rotation.x < -_maxAngle
        //         ? 0
        //         :  GetAngleByOffset(direction.y);
        //     _rigidbody.angularVelocity = new Vector3(
        //         0,
        //         0,
        //         angularSpeed);
        //     if (angularSpeed == 0)
        //     {
        //         Debug.Log("ZERO!");
        //     }
        //     var newPlatform = Instantiate(_platform, _spawner.transform.position, _platform.transform.rotation);
        //     newPlatform.transform.Rotate(0, 0, -transform.rotation.eulerAngles.x);
        // }

        private void Move(Vector3 direction)
        {
           // var offset = Project(direction.normalized) * (_speed * Time.deltaTime);
            var offset = direction.normalized * (_speed * Time.deltaTime);
            _rigidbody.MovePosition(_rigidbody.position + offset);
        }

        private void FixedUpdate()
        {
           
        }

        private Vector3 Project(Vector3 forward)
        {
            return forward - Vector3.Dot(forward, _normal) * _normal;
        }
    
        public Vector3 Direction => Project(transform.forward);
        
        
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawWireCube(transform.position + Vector3.up / 2, Vector3.one);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + _normal * 5);
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + Project(transform.forward) * 5);
        }
    }
}