﻿using System;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Model
{
    public class RoadGenerator : MonoBehaviour
    {
        [SerializeField] private Road _road;
        [SerializeField] private PlatformsStack _platformsStack;
        [SerializeField] private PresentBox _presentBox;
        [SerializeField] private BarrierArea _barrierArea;

        [Header("Generator parameters")] 
        [SerializeField] private float _minRoadLength;
        [SerializeField] private float _maxRoadLength;

        private Vector3 _spawnPosition;
        private float _boundsSize;

        private void Start()
        {
            _spawnPosition = Vector3.forward * 20;
            _boundsSize = _road.GetComponent<MeshFilter>().mesh.bounds.size.z;
            
            for (var roadIndex = 0; roadIndex < 10; roadIndex++)
            {
                SpawnObject(_road.gameObject);
                SpawnObject(_barrierArea.gameObject);
            }    
        }

        private void FixedUpdate()
        {
#if UNITY_EDITOR
            if (Input.GetKey("space"))
            {
                SpawnObject(_road.gameObject);
                SpawnObject(_barrierArea.gameObject);
            }    
#endif
        }

        private void SpawnObject(GameObject spawnObject)
        {
            var size = Random.Range(_minRoadLength, _maxRoadLength + 1);
            _spawnPosition += Vector3.forward * size / 2;
            var newObject = Instantiate(spawnObject, _spawnPosition, Quaternion.identity);

            var localScale = newObject.transform.localScale;
            localScale = new Vector3(localScale.x, localScale.y, size);
            newObject.transform.localScale = localScale;

            _spawnPosition += Vector3.forward * size / 2;
        }    
    }
}