﻿using UnityEngine;
using UnityEngine.Events;

namespace Components
{
    public class PlayerInput : MonoBehaviour
    {
        private Vector3 _tapPosition;

        public UnityAction<Vector2> DirectionChanged;
        public UnityAction PointerUp;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _tapPosition = Input.mousePosition;
                
            }
        
            if (Input.GetMouseButton(0))
            {
                DirectionChanged?.Invoke(Input.mousePosition - _tapPosition);
            }
        
            if (Input.GetMouseButtonUp(0))
            {
                PointerUp?.Invoke();
            }
        }
    }
}