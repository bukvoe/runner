﻿using System;
using Components;
using UnityEngine;

namespace Player.States
{
    public class Surf : PlayerState
    {
        [SerializeField] private PlayerInput _playerInput;
        [SerializeField] private float _speed;
        [SerializeField] private Platform _platform;

        private float _distanceBetweenPlatforms;
        private Vector3 _prevPosition;
        private Platform _prevObject;
        
        private void OnEnable()
        {
            _playerInput.DirectionChanged += OnDirectionChanged;
            
            _distanceBetweenPlatforms = _platform.transform.localScale.x * 1.25f;
            _prevPosition = transform.position;
            _prevObject = _platform;
        }
        
        private void OnDisable()
        {
            if (_playerInput != null) 
                _playerInput.DirectionChanged -= OnDirectionChanged;
        }

        private void OnDirectionChanged(Vector2 direction)
        {
            
            Debug.Log(Rigidbody);
            
            Rigidbody.velocity = Vector3.right * _speed + (direction.y > 0 ? Vector3.up : Vector3.down);
            //_rigidbody.angularVelocity += direction.y > 0 ? Vector3.right : Vector3.left;
            
            
            var distance = Vector2.Distance(transform.position, _prevPosition);
            while (distance >= _distanceBetweenPlatforms)
            {
                var vector2Distance = transform.position - _prevPosition;
                var coeff = _distanceBetweenPlatforms / distance;
                _prevPosition += vector2Distance * coeff;
                
                
                var platform = Instantiate(_platform, _prevPosition, _platform.transform.rotation);

                Vector3 prevPos = _prevObject.GetComponentInChildren<EndPoint>().gameObject.transform.position;

                Vector3 angleVector = _prevPosition - prevPos;

                var x = Vector2.Angle(angleVector, Vector2.right);
                if (direction.y > 0)
                {
                    x *= -1;
                }

                platform.transform.Rotate(x, 0, 0);

                _prevObject = platform;
                distance -= _distanceBetweenPlatforms;

                if (Rigidbody.velocity.y < 0)
                {
                    Debug.Log($"Velocity y: {Rigidbody.velocity.y}");
                    //Debug.Break();
                }
            }
        }
    }
}