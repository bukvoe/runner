using System;
using UnityEngine;

namespace Player
{
    public abstract class PlayerTransition : MonoBehaviour
    {
        [SerializeField] private PlayerState _targetState;

        public PlayerState TargetState => _targetState;
        public bool NeedTransition { get; protected set; }

        public void OnEnable()
        {
            NeedTransition = false;
            Enable();
        }

        public abstract void Enable();
    }
}
