using System.Linq;
using UnityEngine;

namespace Player
{
    public abstract class PlayerState : MonoBehaviour
    {
        [SerializeField] private PlayerTransition[] _transitions;

        protected Rigidbody Rigidbody { get; private set; }
        protected Animator Animator { get; private set; }

        public void Enter(Rigidbody rigidbody, Animator animator)
        {
            if (enabled) return;
        
            Rigidbody = rigidbody;
            Debug.Log("ENTER " + Rigidbody);
            Animator = animator;

            enabled = true;

            foreach (var transition in _transitions)
            {
                transition.enabled = true;
            }
        }
    
        public void Exit()
        {
            if (!enabled) return;
        
            foreach (var transition in _transitions)
            {
                transition.enabled = false;
            }

            enabled = false;
        }

        public PlayerState GetNextState()
        {
            return (from transition in _transitions where transition.NeedTransition select transition.TargetState).FirstOrDefault();
        }
    }
}
