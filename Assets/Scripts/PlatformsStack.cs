using UnityEngine;

public class PlatformsStack : CombineMeshes
{
    [Header("Stack material")]
    [SerializeField] private Material _material;
    protected override void Start()
    {
        base.Start();
        GetComponent<MeshRenderer>().material = _material;
    }
}
