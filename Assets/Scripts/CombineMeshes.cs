﻿using System.ComponentModel;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class CombineMeshes : MonoBehaviour
{
    protected virtual void Start()
    {
        var spawnPosition = transform.position;
        var meshFilters = GetComponentsInChildren<MeshFilter>();
        var combine = new CombineInstance[meshFilters.Length];

        transform.position = Vector3.zero;

        for (var meshIndex = 1; meshIndex < meshFilters.Length; meshIndex++)
        {
            combine[meshIndex].mesh = meshFilters[meshIndex].sharedMesh;
            combine[meshIndex].transform = meshFilters[meshIndex].transform.localToWorldMatrix;
            meshFilters[meshIndex].gameObject.SetActive(false);
        }

        transform.GetComponent<MeshFilter>().sharedMesh = new Mesh();
        transform.GetComponent<MeshFilter>().sharedMesh.CombineMeshes(combine);
        transform.position = spawnPosition;
        transform.gameObject.SetActive(true);
    }
}
